import flask
from flask.blueprints import Blueprint
from database.db import mysql


ocene_blueprint = Blueprint('ocene_blueprint', __name__)

# APPLICATION PROGRAMMING INTERFACE
@ocene_blueprint.route("/ocene", methods=['GET'])
def dobavi_ocene():
    cursor = mysql.get_db().cursor()
    cursor.execute('SELECT * FROM ocena ORDER BY datum_ocene')
    ocene = cursor.fetchall()
    return flask.jsonify(ocene), 200

@ocene_blueprint.route("/oceneStudenti", methods=['GET'])
def dobavi_oceneStudenti():
    cursor = mysql.get_db().cursor()
    cursor.execute('SELECT * FROM ocena ORDER BY student_id')
    ocene = cursor.fetchall()
    return flask.jsonify(ocene), 200

@ocene_blueprint.route("/ocene/<int:id_ocene>", methods=['GET'])
def dobavi_ocenu(id_ocene):
    cursor = mysql.get_db().cursor()
    cursor.execute('SELECT * FROM ocena WHERE id= %s', (id_ocene))
    ocena = cursor.fetchone()
    return flask.jsonify(ocena), 200

#   TODO Srediti brisanje foreign keyova.
@ocene_blueprint.route("/ocene/<int:id_ocene>", methods=['DELETE'])
def obrisi_ocenu(id_ocene):
    db = mysql.get_db()
    cursor = mysql.get_db().cursor()
    cursor.execute('DELETE FROM ocena WHERE id= %s', (id_ocene))
    db.commit()
    return flask.jsonify({'status': 'ok'}), 204


@ocene_blueprint.route("/ocene/<int:id_ocene>", methods=['PUT'])
def izmeni_ocenu(id_ocene):
    data = flask.request.get_json()
    data["id"] = id_ocene
    print(data)
    db = mysql.get_db()
    cursor = mysql.get_db().cursor()
    cursor.execute(
        "UPDATE ocena SET student_id= %(student_id)s, zadatak_id= %(zadatak_id)s, ocena=%(ocena)s, datum_ocene=%(datum_ocene)s  WHERE id=%(id)s", data)
    db.commit()
    return flask.jsonify({'status': 'ok'}), 200


@ocene_blueprint.route("/ocene", methods=["POST"])
def dodaj_ocenu():
    data = flask.request.json
    db = mysql.get_db()
    cursor = db.cursor()
    print(flask.request.json)
    cursor.execute(
        "INSERT INTO ocena(student_id, zadatak_id, ocena, datum_ocene) VALUES(%(student_id)s, %(zadatak_id)s, %(ocena)s, %(datum_ocene)s)", data)
    db.commit()
    return flask.jsonify({"status": "Resource created."}), 204
