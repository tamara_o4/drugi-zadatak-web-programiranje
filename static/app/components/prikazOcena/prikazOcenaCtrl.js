(function(angular) {
    let app = angular.module("app").controller("prikazOcenaCtrl", [
      "$http",
      "$state",
      function($http, $state) {
        let that = this;

        this.dobaviOcene = function() {
          $http.get("/api/ocene").then(
            function(response) {
              that.ocene = response.data;
  
              console.log(that.zadaci);
            },
            function(reason) {
              console.log(reason);
            }
          );
        };

        this.dobaviZadatke = function() {
            $http.get("/api/dobaviSveZadatke").then(
              function(response) {
                that.zadaci = response.data;
    
                console.log(that.zadaci);
              },
              function(reason) {
                console.log(reason);
              }
            );
          };
  
        this.dobaviSveStudente = function() {
        $http.get("/api/dobaviSveStudente").then(
            function(response) {
            that.students = response.data;
            console.log(response.data);
            },
            function(reason) {
            console.log(reason);
            }
        );
        };

        this.sortirajPoStudentima = function() {
          $http.get("/api/oceneStudenti").then(
            function(response) {
              that.ocene = response.data;
              console.log(that.zadaci);
            },
            function(reason) {
              console.log(reason);
            }
          );
        };

       
        this.dobaviZadatke();
        this.dobaviSveStudente();
        this.dobaviOcene();
    
        this.wrapperFunction = function() {
          this.sortirajPoStudentima();
        };
  

      }
    ]);
  })(angular);  