(function(angular) {
    let app = angular.module("app").controller("unosOcenaCtrl", [
      "$http",
      "$state",
      function($http, $state) {
        let that = this;
        this.novaOcena = {
          student_id: "",
          zadatak_id: "",
          ocena: "",
          datum_ocene: ""
        };
  
        this.dobaviSveStudente = function() {
            $http.get("/api/dobaviSveStudente").then(
              function(response) {
                that.students = response.data;
                console.log(response.data);
              },
              function(reason) {
                console.log(reason);
              }
            );
        };

        this.dobaviZadatke = function() {
            $http.get("/api/dobaviSveZadatke").then(
              function(response) {
                that.zadaci = response.data;
    
                console.log(that.zadaci);
              },
              function(reason) {
                console.log(reason);
              }
            );
        };
  
        this.srediDatum = function() {
          mysqlDateFormat = that.novaOcena.datum_ocene
            .toISOString()
            .slice(1, 19)
            .replace("T", " ");
          console.log(mysqlDateFormat);
          that.novaOcena.datum_ocene = mysqlDateFormat;
        };
  
        this.unosOcena = function() {
          that.srediDatum();
          $http.post("/api/ocene", that.novaOcena).then(
            function(response) {
              console.log("Uspijesno...");
              $state.go("home");
            },
            function(reason) {
              console.log(reason);
            }
          );
        };
  
        this.dobaviZadatke();
        this.dobaviSveStudente();
      }
    ]);
  })(angular);
  